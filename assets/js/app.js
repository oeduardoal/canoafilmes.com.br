import $ from "jquery";
window.jQuery = window.$ = $;
import TypeIt from "typeit";
import ScrollReveal from "scrollreveal";
import Tether from "tether";
import Popper from "popper.js";
import bootstrap from "bootstrap";

import * as basicLightbox from "basiclightbox";

const showModal = (tipo, id, title, descricao, link) => {
  let iframe;

  if (tipo == "vimeo")
    iframe = `<iframe src="https://player.vimeo.com/video/${id}?title=1&byline=1&portrait=1&transparent=1&autoplay=1" frameborder="0"></iframe>`;
  else if (tipo == "youtube")
    iframe = `<iframe src="https://www.youtube.com/embed/${id}" frameborder="0"></iframe>`;

  iframe = ` ${iframe}
            <div class="modal-portfolio">
              <span class="modal-portfolio-title">
                ${title}
              </span>
              <span class="modal-portfolio-desc">
                ${descricao}
              </span>
              <a href="${link}" class="modal-portfolio-link">Veja mais!</a>
            </div>`;

  const instance = basicLightbox.create(iframe, {
    closable: true
  });

  instance.show();

};

$(document).ready(() => {
  console.log("Developed by: Eduardo Almeida");

  $("#main-banner.banner-portfolio .owl-carousel").owlCarousel({
    items: 1,
    loop: false
  });

  $('a[href*="#"]:not([href="#"])').click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
      this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html, body").animate(
          {
            scrollTop: target.offset().top
          },
          1000
        );
        return false;
      }
    }
  });
  $("#main-clientes-parceiros .owl-carousel").owlCarousel({
    items: 5,
    loop: true,
    margin: 100,
    autoplay: true,
    autoplaySpeed: 1000,
    responsive: {
      0: {
        items: 1,
        margin: 10
      },
      480: {
        items: 3,
        margin: 10
      },
      800: {
        items: 4,
        margin: 20
      }
    }
  });

  $(".masonry-brick a").on("click", function (ev) {
    ev.preventDefault();
    let tipo = $(this).attr("data-tipo");
    let id = $(this).attr("data-id");
    let title = $(this).attr("data-title");
    let descricao = $(this).attr("data-descricao");
    let link = $(this).attr("data-link");

    showModal(tipo, id, title, descricao, link);
  });
  $('#main-header a.icon').click(function (event) {
    $('#main-menu, #main-social').toggleClass('hidden-menu');
  })

});
